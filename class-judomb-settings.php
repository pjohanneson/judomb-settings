<?php
/**
 * Settings core class file.
 *
 * @package judomb\settings
 */

/**
 * Settings core class.
 *
 * @since 1.0.0 Introduced.
 */
class JudoMB_Settings {

	const PREFIX = '_jmb_';

	/**
	 * Class constructor.
	 *
	 * @return void
	 * @since 1.0.0
	 */
	function __construct() {
		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );
		add_action( 'admin_init', array( $this, 'settings_init' ) );
	}

	/**
	 * Adds the submenu to the Settings menu.
	 *
	 * @return void
	 * @since 1.0.0
	 */
	public function add_admin_menu() {
		add_submenu_page( 'options-general.php', 'Contact Footer', 'Contact Footer', 'edit_others_pages', 'judomb-settings', array( $this, 'options_page' ) );
	}

	/**
	 * Prints out the menu page.
	 *
	 * @return void
	 * @since 1.0.0
	 */
	public static function options_page() {
		?>
		<h2>Contact Footer</h2>
		<form action="options.php" method="post">
		<?php
			settings_fields( JudoMB_Settings::PREFIX . 'contact_footer' );
			do_settings_sections( JudoMB_Settings::PREFIX . 'contact_footer' );
			submit_button();
		?>
		</form>
		<?php

	}

	/**
	 * Initializes the settings.
	 *
	 * @return void
	 * @since 1.0.0
	 */
	public function settings_init() {
		register_setting( JudoMB_Settings::PREFIX . 'contact_footer', JudoMB_Settings::PREFIX . 'contact_footer_settings' );
		add_settings_section(
			JudoMB_Settings::PREFIX . 'contact_footer_section',
			__( 'Contact Footer', 'judomb' ),
			array( $this, 'settings_section_cb' ),
			JudoMB_Settings::PREFIX . 'contact_footer'
		);
		add_settings_field(
			JudoMB_Settings::PREFIX . 'contact_footer_content',
			__( 'Enter the contact info', 'judomb' ),
			array( $this, 'contact_footer_content_render' ),
			JudoMB_Settings::PREFIX . 'contact_footer',
			JudoMB_Settings::PREFIX . 'contact_footer_section'
		);
	}

	/**
	 * Renders the settings section.
	 *
	 * @return void
	 * @since 1.0.0
	 */
	public static function settings_section_cb() {
		echo esc_html__( 'Enter the contact info', 'judomb' );
	}

	/**
	 * Render the "Contact Footer" selection box.
	 *
	 * @return void
	 * @since  1.0.0 Introduced.
	 */
	public function contact_footer_content_render() {
		$_option = get_option( JudoMB_Settings::PREFIX . 'contact_footer_settings', array() );
		$option = '';
		if ( ! empty( $_option['contact_footer_text'] ) ) {
			$option = esc_html( $_option['contact_footer_text'] );
		}
		$input = '<input type="text" name="' . JudoMB_Settings::PREFIX . 'contact_footer_settings[contact_footer_text]"';
		if ( ! empty( $option ) ) {
			$input .= ' value="' . $option . '"';
		}
		$input .= ' />';
		echo $input; // wpcs: xss ok.
	}

}

new JudoMB_Settings;
