<?php
/**
 * Plugin loader file.
 *
 * @package judomb\settings
 */

/**
 * Plugin Name: Judo MB Settings
 * Description: Provides various site settings (initially: the contact footer).
 * Version:     1.0.0
 */

require 'class-judomb-settings.php';
